package com.holiash.view;

import com.holiash.model.Sentence;
import com.holiash.model.Word;
import java.util.List;
import java.util.Map;

public class View {

  public void show(String msg) {
    System.out.println(msg);
  }

  public <T> void show(String msg, T t) {
    System.out.println(msg + "\t" + t);
  }

  public <T> void show(String msg, List<T> list) {
    System.out.println(msg);
    list.forEach(System.out::println);
  }

  public <K, V> void show(String msg, Map<K, V> map) {
    System.out.println(msg);
    map.forEach((key, value) -> System.out.println(key + "=" + value));
  }

  public void showWordsSortedByAlphabet(String msg, List<Word> words) {
    System.out.println(msg);
    for (int i = 0; i < words.size() - 1; i++) {
      System.out.print(words.get(i) + "; ");
      if (words.get(i).getWord().charAt(0) != words.get(i + 1).getWord().charAt(0)) {
        System.out.println();
      }
    }
    System.out.println(words.get(words.size() - 1));
  }

  public void showWordsEntries(String msg, Map<Sentence, Map<String, Long>> map) {
    System.out.println(msg);
    map.forEach((key, value) -> {
      System.out.println(key);
      value.forEach((s, l) -> System.out.println(s + "=" + l));
    });
  }
}
