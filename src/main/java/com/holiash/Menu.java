package com.holiash;

import com.holiash.controller.TextHandlerController;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Menu {

  private final Scanner scanner = new Scanner(System.in);
  private Map<Integer, Func> menu;
  private ResourceBundle bundle = ResourceBundle.getBundle("menu");
  private TextHandlerController controller;
  private List<String> menuList;

  public Menu(TextHandlerController controller) {
    menu = new LinkedHashMap<>();
    this.controller = controller;
    setEnMenu();
  }

  public void intiMenu() {
    menu.put(1, this::largestNumberOfSentencesWithSameWords);
    menu.put(2, this::sortedSentencesByWordsNum);
    menu.put(3, this::uniqueWordFromFirstSentence);
    menu.put(4, this::questionableSentenceWords);
    menu.put(5, this::swapWords);
    menu.put(6, this::sortedWords);
    menu.put(7, this::sortByVowelFrequency);
    menu.put(8, this::sortByFirstConsonant);
    menu.put(9, this::sortByLetterFrequency);
    menu.put(10, this::countWordsEntries);
    menu.put(11, this::deleteSubstring);
    menu.put(12, this::removeWords);
    menu.put(13, this::sortWordsByChar);
    menu.put(14, this::replaceWithSubstring);
    menu.put(15, this::setUkMenu);
    menu.put(16, this::setEnMenu);
  }

  public int getSize() {
    return this.menu.size();
  }

  public void printMenu() {
    this.menuList.forEach(System.out::println);
  }

  private void setUkMenu() {
    bundle = ResourceBundle.getBundle("menu", new Locale("uk"));
    menuList = new ArrayList<>();
    Enumeration<String> keys = bundle.getKeys();
    String key;
    while (keys.hasMoreElements()) {
      key = keys.nextElement();
      this.menuList.add(bundle.getString(key));
    }
  }

  private void setEnMenu() {
    bundle = ResourceBundle.getBundle("menu", new Locale("en"));
    menuList = new ArrayList<>();
    String key;
    Enumeration<String> keys = bundle.getKeys();
    while (keys.hasMoreElements()) {
      key = keys.nextElement();
      this.menuList.add(bundle.getString(key));
    }
  }

  private void largestNumberOfSentencesWithSameWords() {
    this.controller.largestNumberOfSentencesWithSameWords();
  }

  private void sortedSentencesByWordsNum() {
    this.controller.sortedSentencesByWordsNum();
  }

  private void uniqueWordFromFirstSentence() {
    this.controller.uniqueWordFromFirstSentence();
  }

  private void questionableSentenceWords() {
    System.out.println("Input length: ");
    int length = scanner.nextInt();
    this.controller.questionableSentenceWords(length);
  }

  private void swapWords() {
    this.controller.swapWords();
  }

  private void sortedWords() {
    this.controller.sortedWords();
  }

  private void sortByVowelFrequency() {
    this.controller.sortByVowelFrequency();
  }

  private void sortByFirstConsonant() {
    this.controller.sortByFirstConsonant();
  }

  private void sortByLetterFrequency() {
    System.out.println("Input char: ");
    String s = scanner.next();
    char c = s.charAt(0);
    this.controller.sortByLetterFrequency(c);
  }

  private void countWordsEntries() {
    System.out.println("Input words: ");
    List<String> words = Arrays.asList(scanner.nextLine().split(" "));
    this.controller.countWordsEntries(words);
  }

  private void deleteSubstring() {
    System.out.println("Input two chars: ");
    char c1 = scanner.next().charAt(0);
    char c2 = scanner.next().charAt(0);
    this.controller.deleteSubstring(c1, c2);
  }

  private void removeWords() {
    System.out.println("Input length: ");
    int length = scanner.nextInt();
    this.controller.removeWords(length);
  }

  private void sortWordsByChar() {
    System.out.println("Input char: ");
    char c = scanner.next().charAt(0);
    this.controller.sortWordsByChar(c);
  }

  private void replaceWithSubstring() {
    System.out.println("Input sentence number, length of word and string to insertion: ");
    int sentenceNum = scanner.nextInt();
    int length = scanner.nextInt();
    String toInsert = scanner.nextLine();
    this.controller.replaceWithSubstring(sentenceNum, length, toInsert);
  }

  public void execute(int num) {
    this.menu.get(num).run();
  }
}
