package com.holiash;

import java.util.Scanner;

public class Run {

  private static final Scanner scanner = new Scanner(System.in);

  public static void run(Menu menu) {
    menu.intiMenu();
    menu.printMenu();
    int choice = scanner.nextInt();
    while (choice > 0 && choice < 17) {
      menu.execute(choice);
      System.out.println();
      menu.printMenu();
      choice = scanner.nextInt();
    }
  }

}
