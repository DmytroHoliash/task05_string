package com.holiash.controller;

import com.holiash.model.Sentence;
import com.holiash.model.TextHandler;
import com.holiash.model.Word;
import com.holiash.view.View;
import java.util.List;

public class TextHandlerController {

  private final TextHandler handler;
  private final View view;

  public TextHandlerController(TextHandler handler, View view) {
    if (handler == null) {
      throw new NullPointerException();
    }
    this.handler = handler;
    this.view = view;
  }

  public void largestNumberOfSentencesWithSameWords() {
    Long number = this.handler.largestNumberOfSentencesWithSameWords();
    this.view.show("Largest number of sentences with same words = ", number);
  }

  public void sortedSentencesByWordsNum() {
    List<Sentence> sentences = this.handler.sortedSentencesByWordsNum();
    this.view.show("Sentences sorted by words number: ", sentences);
  }

  public void uniqueWordFromFirstSentence() {
    Word word = this.handler.uniqueWordFromFirstSentence();
    this.view.show("unique word from first sentence", word.getWord());
  }

  public void questionableSentenceWords(int length) {
    List<Word> words = this.handler.questionableSentenceWords(length);
    this.view.show("Words from questionable sentences with length=" + length, words);
  }

  public void swapWords() {
    this.view.show("Sentences after swapping words: ", this.handler.swapWords());
  }

  public void sortedWords() {
    this.view.showWordsSortedByAlphabet("Sorted words: ", this.handler.sortedWords());
  }

  public void sortByVowelFrequency() {
    this.view.show("Sorted by vowel frequency: ", this.handler.sortByVowelFrequency());
  }

  public void sortByFirstConsonant() {
    this.view.show("Sorted by first consonant: ", this.handler.sortByFirstConsonant());
  }

  public void sortByLetterFrequency(char c) {
    this.view.show("Sorted by Letter frequency: ", this.handler.sortByLetterFrequency(c));
  }

  public void countWordsEntries(List<String> words) {
    this.view.showWordsEntries("Words entries in each sentence: ",
        this.handler.countWordsEntries(words));
  }

  public void deleteSubstring(char start, char finish) {
    try {
      this.view.show("delete substring from " + start + " to " + finish + " symbols",
          this.handler.deleteSubstring(start, finish));
    } catch (Exception e) {
      System.out.println("cant remove");
    }
  }

  public void removeWords(int length) {
    this.view.show("words removed: \n", this.handler.removeWords(length));
  }

  public void sortWordsByChar(char c) {
    this.view.show("Sorted by " + c + " occurrences", this.handler.sortWordsByChar(c));
  }

  public void replaceWithSubstring(int sentenceNum, int length, String toInsert) {
    this.view.show("Sentence after replacing: \n",
        this.handler.replaceWithSubstring(sentenceNum, length, toInsert));
  }
}
