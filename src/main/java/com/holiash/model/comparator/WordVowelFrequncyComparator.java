package com.holiash.model.comparator;

import com.holiash.model.Word;
import java.util.Comparator;

public class WordVowelFrequncyComparator implements Comparator<Word> {

  @Override
  public int compare(Word o1, Word o2) {
    double result = (1.0 * o1.getNumOfVowel() / o1.getWordLength())
        - (1.0 * o2.getNumOfVowel() / o2.getWordLength());
    if (result < 0) {
      return -1;
    } else if (result > 0) {
      return 1;
    } else {
      return 0;
    }
  }
}
