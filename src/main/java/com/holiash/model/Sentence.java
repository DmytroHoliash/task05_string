package com.holiash.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.holiash.model.Const.*;

public class Sentence {

  private String sentenceText;
  private List<Word> words;
  private String sentenceType;
  private int wordsNumber;

  Sentence(String sentence, char endChar) {
    this.sentenceText = sentence;
    this.words = new ArrayList<>();
    List<String> wordsList = Arrays.asList(sentence.split(PUNCTUATION_MARK_REGEX));
    for (String w : wordsList) {
      words.add(new Word(w));
    }
    if (endChar == '.') {
      this.sentenceType = "declarative";
    } else if (endChar == '?') {
      this.sentenceType = "questionable";
    } else {
      this.sentenceType = "exclamation";
    }
    this.wordsNumber = this.words.size();
  }

  public String getSentenceText() {
    return sentenceText;
  }

  public List<Word> getWords() {
    return words;
  }

  public String getSentenceType() {
    return sentenceType;
  }

  public int getWordsNumber() {
    return wordsNumber;
  }

  boolean contains(Word word) {
    if (word == null) {
      throw new NullPointerException();
    }
    for (Word w : this.words) {
      if (w.getWord().equalsIgnoreCase(word.getWord())) {
        return true;
      }
    }
    return false;
  }

  private String longestWord() {
    Map<Word, Integer> wordLength = new LinkedHashMap<>();
    List<Word> distinctWords = words.stream().distinct().collect(Collectors.toList());
    for (Word word : distinctWords) {
      wordLength.put(word, word.getWordLength());
    }
    Integer max = wordLength.values().stream().max(Integer::compareTo).get();
    List<Word> longest = wordLength.keySet().stream()
        .filter(k -> wordLength.get(k).equals(max))
        .limit(1)
        .collect(Collectors.toList());
    return longest.get(0).getWord();
  }

  private String firstWordWithVowelLetter() {
    List<Word> first = words.stream().filter(s -> s.getWord().matches("[aAuUiIoO]\\w+"))
        .limit(1)
        .collect(Collectors.toList());
    return first.get(0).getWord();
  }

  void swapWords() {
    String first = this.firstWordWithVowelLetter();
    String second = this.longestWord();
    if (!first.equals(second)) {
      int index1 = sentenceText.indexOf(first);
      int index2 = sentenceText.indexOf(second);
      if (index1 < index2) {
        sentenceText = sentenceText.substring(0, index1) + second
            + sentenceText.substring(index1 + first.length(), index2)
            + first + sentenceText.substring(index2 + second.length());
      } else {
        sentenceText = sentenceText.substring(0, index2) + first
            + sentenceText.substring(index2 + second.length(), index1)
            + first + sentenceText.substring(index1 + first.length());
      }
    }
  }

  public String replaceWithSubstring(int length, String toInsert) {
    return this.sentenceText.replaceAll("\\s+[a-zA-z]{" + length + "}\\s+", toInsert);
  }

  public long countWords(String wordToCount) {
    return this.words.stream().filter(w -> w.getWord().equalsIgnoreCase(wordToCount)).count();
  }

  public String replaceSubstring(char start, char finish) {
    return this.sentenceText.substring(0, sentenceText.indexOf(start))
        + this.sentenceText.substring(this.sentenceText.lastIndexOf(finish));
  }

  @Override
  public String toString() {
    return this.sentenceText + "  ---  " + this.sentenceType;
  }
}
