package com.holiash.model;

public class Const {

  public static final String END_OF_SENTENCE_REGEX = "[^.?!]*[.?!]";
  public static final String PUNCTUATION_MARK_REGEX = "\\s*[,:;-]\\s*|\\s";

}
