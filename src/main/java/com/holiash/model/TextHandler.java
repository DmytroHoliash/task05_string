package com.holiash.model;

import com.holiash.model.comparator.WordVowelFrequncyComparator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.holiash.model.Const.*;
import static com.holiash.logger.ModelLogger.*;

public class TextHandler {

  private String text;
  private List<Sentence> sentences;
  private List<Word> words;

  public TextHandler(String text) {
    this.text = text.replaceAll("\\s+", " ");
    this.sentences = new ArrayList<>();
    this.words = new ArrayList<>();
    splitToSentences();
    splitToWords();
  }

  private void splitToSentences() {
    Pattern p = Pattern.compile(END_OF_SENTENCE_REGEX);
    Matcher m = p.matcher(text);
    while (m.find()) {
      this.sentences.add(
          new Sentence(text.substring(m.start(), m.end() - 1).trim(), text.charAt(m.end() - 1)));
    }
  }

  private void splitToWords() {
    for (Sentence s : this.sentences) {
      this.words.addAll(s.getWords());
    }
  }

  public Long largestNumberOfSentencesWithSameWords() {
    LOGGER.info(TextHandler.class + ".largestNumberOfSentencesWithSameWords() proceeding");
    List<Word> uniqueWords = this.words.stream().distinct().collect(Collectors.toList());
    Map<Word, Long> numberOfSentencesThatContainsWord = new HashMap<>();
    for (Word word : words) {
      long number = sentences.stream().filter(sentence -> sentence.contains(word))
          .count();
      numberOfSentencesThatContainsWord.put(word, number);
    }
    Optional<Long> max = numberOfSentencesThatContainsWord.values().stream().max(Long::compare);
    return max.get();
  }

  public List<Sentence> sortedSentencesByWordsNum() {
    LOGGER.info(TextHandler.class + ".sortedSentencesByWordsNum() proceeding");
    return this.sentences.stream().sorted(Comparator.comparingInt(Sentence::getWordsNumber))
        .collect(Collectors.toList());
  }

  public Word uniqueWordFromFirstSentence() {
    LOGGER.info("uniqueWordFromFirstSentence() proceeding");
    List<Sentence> sentenceWithoutFirst = new ArrayList<>(sentences.subList(1, sentences.size()));
    for (Word word : this.sentences.get(0).getWords()) {
      if (sentenceWithoutFirst.stream().noneMatch(sentence -> sentence.contains(word))) {
        return word;
      }
    }
    return null;
  }

  public List<Word> questionableSentenceWords(final int length) {
    LOGGER.info(TextHandler.class + ".questionableSentenceWords() proceeding");
    List<Sentence> questionable = this.sentences.stream()
        .filter(s -> s.getSentenceText().equals("questionable"))
        .collect(Collectors.toList());
    return questionable.stream()
        .map(Sentence::getWords)
        .flatMap(Collection::stream)
        .filter(w -> w.getWordLength() == length)
        .distinct()
        .collect(Collectors.toList());
  }

  public List<Sentence> swapWords() {
    LOGGER.info(TextHandler.class + ".swapWords() proceeding");
    this.sentences.forEach(Sentence::swapWords);
    return this.sentences;
  }

  public List<Word> sortedWords() {
    LOGGER.info(TextHandler.class + ".sortedWords() proceeding");
    return words.stream().sorted().collect(Collectors.toList());
  }

  public List<Word> sortByVowelFrequency() {
    LOGGER.info(TextHandler.class + ".sortByVowelFrequency() proceeding");
    return this.words.stream()
        .distinct()
        .sorted(new WordVowelFrequncyComparator())
        .collect(Collectors.toList());
  }

  public List<Word> sortByFirstConsonant() {
    LOGGER.info(TextHandler.class + ".sortByFirstConsonant() proceeding");
    return this.words.stream().filter(word -> word.getWord().matches("[aAuUiIoO]\\w+"))
        .sorted(Comparator.comparing(Word::getFirstConsonant))
        .collect(Collectors.toList());
  }

  public Map<String, Integer> sortByLetterFrequency(final char c) {
    LOGGER.info(TextHandler.class + ".sortByLetterFrequency() proceeding");
    Map<String, Integer> charFrequency = this.words.stream()
        .distinct()
        .collect(Collectors.toMap(Word::getWord, (Word word) -> word.charCounter(c)));
    return charFrequency.entrySet().stream()
        .sorted(Map.Entry.comparingByKey())
        .sorted(Map.Entry.comparingByValue())
        .collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e2,
            LinkedHashMap::new));
  }

  public Map<Sentence, Map<String, Long>> countWordsEntries(List<String> words) {
    LOGGER.info(TextHandler.class + ".countWordsEntries() proceeding");
    Map<Sentence, Map<String, Long>> map = new HashMap<>();
    for (Sentence sen : this.sentences) {
      map.put(sen, words.stream().collect(Collectors.toMap(w -> w, sen::countWords)));
    }
    return map;
  }

  public List<String> deleteSubstring(char start, char finish) {
    return this.sentences.stream()
        .map(sentence -> sentence.replaceSubstring(start, finish)).collect(Collectors.toList());
  }

  public String removeWords(int length) {
    String regex = "\\s+[^aAeEuUiIoO][a-z]{" + (length - 1) + "}\\s+";
    return this.text.replaceAll(regex, " ");
  }

  public List<Word> sortWordsByChar(char c) {
    return this.words.stream()
        .sorted((w1, w2) -> w1.getWord().compareToIgnoreCase(w2.getWord()))
        .sorted(Comparator.comparingInt(w -> w.charCounter(c)))
        .collect(Collectors.toList());
  }

  public String replaceWithSubstring(int sentenceNum, int length, String toInsert) {
    if (sentenceNum < 0 || sentenceNum >= this.sentences.size() || length < 0) {
      throw new IllegalArgumentException();
    }
    return this.sentences.get(sentenceNum).replaceWithSubstring(length, toInsert);
  }
}
