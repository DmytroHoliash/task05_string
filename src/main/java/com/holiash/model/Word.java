package com.holiash.model;

import java.util.Objects;

public class Word implements Comparable<Word> {

  private String word;
  private int numOfVowel;
  private int numOfConsonants;
  private int wordLength;
  private String firstConsonant;

  public Word(String word) {
    this.word = word;
    numOfVowel = 0;
    numOfConsonants = 0;
    wordLength = word.length();
    countVowelAndConsonantChars();
  }

  public String getWord() {
    return word;
  }

  public int getNumOfVowel() {
    return numOfVowel;
  }

  public int getNumOfConsonants() {
    return numOfConsonants;
  }

  public int getWordLength() {
    return wordLength;
  }

  public String getFirstConsonant() {
    return firstConsonant;
  }

  public int charCounter(char c) {
    if (c == '\u0000') {
      throw new RuntimeException("wrong character");
    }
    int counter = 0;
    for (int i = 0; i < wordLength; i++) {
      if (word.charAt(i) == c) {
        counter++;
      }
    }
    return counter;
  }

  @Override
  public int compareTo(Word o) {
    return this.word.compareTo(o.word);
  }

  private void countVowelAndConsonantChars() {
    char[] chars = word.toLowerCase().toCharArray();
    for (char ch : chars) {
      if (ch == 'a' || ch == 'u' || ch == 'i' || ch == 'o' || ch == 'e') {
        numOfVowel++;
      } else {
        if (firstConsonant == null) {
          firstConsonant = String.valueOf(ch);
        }
        numOfConsonants++;
      }
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Word)) {
      return false;
    }
    Word word1 = (Word) o;
    return word.equals(word1.word);
  }

  @Override
  public int hashCode() {
    return Objects.hash(word);
  }

  @Override
  public String toString() {
    return word;
  }
}
