package com.holiash;

@FunctionalInterface
public interface Func {

  void run();
}
